package com.acecom.musicfly.controller;

import com.acecom.musicfly.domain.Album;
import com.acecom.musicfly.service.IAlbumService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/album")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AlbumController {

    private final IAlbumService albumService;

    @GetMapping("/search")
    public List<Album> search(@RequestParam String value) {
        System.out.println(value);
        return albumService.search(value);
    }

    @GetMapping("/info")
    public Album getInfo(@RequestParam String name, @RequestParam String artist) {
        return albumService.getInfo(name, artist);
    }
}
