package com.acecom.musicfly.controller;

import com.acecom.musicfly.domain.Track;
import com.acecom.musicfly.service.ITrackService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/track")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TrackController {

    private final ITrackService trackService;

    @GetMapping("/search")
    public List<Track> search(@RequestParam String value) {
        return trackService.search(value);
    }

    @GetMapping("/info")
    public Track getInfo(@RequestParam String name, @RequestParam String artist) {
        return trackService.getInfo(name, artist);
    }
}
