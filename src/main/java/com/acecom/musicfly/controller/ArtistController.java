package com.acecom.musicfly.controller;

import com.acecom.musicfly.domain.Album;
import com.acecom.musicfly.domain.Artist;
import com.acecom.musicfly.domain.Track;
import com.acecom.musicfly.service.IArtistService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/artist")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ArtistController {

    private final IArtistService artistService;

    @GetMapping("/search")
    public List<Artist> search(@RequestParam String value) {
        return artistService.search(value);
    }

    @GetMapping("/info")
    public Artist getInfo(@RequestParam String name) {
        return artistService.getInfo(name);
    }

    @GetMapping("/topAlbums")
    public List<Album> getTopAlbums(@RequestParam String artist) {
        return artistService.getTopAlbums(artist);
    }

    @GetMapping("/topTracks")
    public List<Track> getTopTracks(@RequestParam String artist) {
        return artistService.getTopTracks(artist);
    }
}
