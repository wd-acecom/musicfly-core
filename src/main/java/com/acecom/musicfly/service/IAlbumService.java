package com.acecom.musicfly.service;

import com.acecom.musicfly.domain.Album;

import java.util.List;

public interface IAlbumService {

    List<Album> search(String value);

    Album getInfo(String albumId, String artist);
}
