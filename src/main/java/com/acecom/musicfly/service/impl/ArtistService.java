package com.acecom.musicfly.service.impl;

import com.acecom.musicfly.domain.Album;
import com.acecom.musicfly.domain.Artist;
import com.acecom.musicfly.domain.Track;
import com.acecom.musicfly.service.IArtistService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ArtistService implements IArtistService {

    private final RestTemplate restTemplate = new RestTemplate();

    private final UriComponentsBuilder _uriComponentsBuilder;
    private final ObjectMapper mapper;

    @Autowired
    public ArtistService(UriComponentsBuilder _uriComponentsBuilder, ObjectMapper mapper) {
        this._uriComponentsBuilder = _uriComponentsBuilder;
        this.mapper = mapper;
    }

    @Override
    public List<Artist> search(String value) {
        UriComponentsBuilder uriComponentsBuilder = (UriComponentsBuilder) _uriComponentsBuilder.clone();

        URI uri = uriComponentsBuilder
                .queryParam("method", "artist.search")
                .queryParam("artist", value)
                .build()
                .toUri();

        ResponseEntity<JsonNode> lastfmResponse = restTemplate.exchange(uri, HttpMethod.GET, null, JsonNode.class);

        try {
            List<Artist> artists = mapper.readValue(Objects.requireNonNull(lastfmResponse.getBody())
                            .get("results")
                            .get("artistmatches")
                            .get("artist").toString(),
                    mapper.getTypeFactory().constructCollectionType(List.class, Artist.class));

            return artists.stream().filter(artist -> !artist.getImage().isEmpty() && artist.getImage() != null).collect(Collectors.toList());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Artist getInfo(String name) {
        UriComponentsBuilder uriComponentsBuilder = (UriComponentsBuilder) _uriComponentsBuilder.clone();

        URI uri = uriComponentsBuilder
                .queryParam("method", "artist.getInfo")
                .queryParam("artist", name)
                .build()
                .toUri();

        ResponseEntity<JsonNode> lastfmResponse = restTemplate.exchange(uri, HttpMethod.GET, null, JsonNode.class);

        try {
            return mapper.readValue(
                    Objects.requireNonNull(lastfmResponse.getBody()).get("artist").toString(),
                    Artist.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Album> getTopAlbums(String artist) {
        UriComponentsBuilder uriComponentsBuilder = (UriComponentsBuilder) _uriComponentsBuilder.clone();

        URI uri = uriComponentsBuilder
                .queryParam("method", "artist.getTopAlbums")
                .queryParam("artist", artist)
                .build()
                .toUri();

        ResponseEntity<JsonNode> lastfmResponse = restTemplate.exchange(uri, HttpMethod.GET, null, JsonNode.class);

        try {
            return mapper.readValue(
                    Objects.requireNonNull(lastfmResponse.getBody()).get("topalbums").get("album").toString(),
                    mapper.getTypeFactory().constructCollectionType(List.class, Album.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Track> getTopTracks(String artist) {
        UriComponentsBuilder uriComponentsBuilder = (UriComponentsBuilder) _uriComponentsBuilder.clone();

        URI uri = uriComponentsBuilder
                .queryParam("method", "artist.getTopTracks")
                .queryParam("artist", artist)
                .build()
                .toUri();

        ResponseEntity<JsonNode> lastfmResponse = restTemplate.exchange(uri, HttpMethod.GET, null, JsonNode.class);

        try {
            return mapper.readValue(
                    Objects.requireNonNull(lastfmResponse.getBody()).get("toptracks").get("track").toString(),
                    mapper.getTypeFactory().constructCollectionType(List.class, Track.class));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
