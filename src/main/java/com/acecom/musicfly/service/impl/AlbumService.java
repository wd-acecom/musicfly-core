package com.acecom.musicfly.service.impl;

import com.acecom.musicfly.domain.Album;
import com.acecom.musicfly.service.IAlbumService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class AlbumService implements IAlbumService {

    private final RestTemplate restTemplate = new RestTemplate();

    private final UriComponentsBuilder _uriComponentsBuilder;
    private final ObjectMapper mapper;

    @Autowired
    public AlbumService(UriComponentsBuilder _uriComponentsBuilder, ObjectMapper mapper) {
        this._uriComponentsBuilder = _uriComponentsBuilder;
        this.mapper = mapper;
    }

    @Override
    public List<Album> search(String searchValue) {
        UriComponentsBuilder uriComponentsBuilder = (UriComponentsBuilder) _uriComponentsBuilder.clone();

        URI uri = uriComponentsBuilder
                .queryParam("method", "album.search")
                .queryParam("album", searchValue)
                .build()
                .toUri();

        ResponseEntity<JsonNode> lastfmResponse = restTemplate.exchange(uri, HttpMethod.GET, null, JsonNode.class);

        try {

            List<Album> albums = mapper.readValue(Objects.requireNonNull(lastfmResponse.getBody())
                            .get("results")
                            .get("albummatches")
                            .get("album").toString(),
                    mapper.getTypeFactory().constructCollectionType(List.class, Album.class));

            return albums.stream().filter(album -> !album.getImage().isEmpty() && album.getImage() != null).collect(Collectors.toList());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Album getInfo(String name, String artist) {
        UriComponentsBuilder uriComponentsBuilder = (UriComponentsBuilder) _uriComponentsBuilder.clone();

        URI uri = uriComponentsBuilder
                .queryParam("method", "album.getinfo")
                .queryParam("album", name)
                .queryParam("artist", artist)
                .build()
                .toUri();

        ResponseEntity<JsonNode> lastfmResponse = restTemplate.exchange(uri, HttpMethod.GET, null, JsonNode.class);

        try {
            return mapper.readValue(
                    Objects.requireNonNull(lastfmResponse.getBody()).get("album").toString(),
                    Album.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
