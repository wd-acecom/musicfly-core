package com.acecom.musicfly.service.impl;

import com.acecom.musicfly.domain.Track;
import com.acecom.musicfly.service.ITrackService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class TrackService implements ITrackService {

    private final RestTemplate restTemplate = new RestTemplate();

    private final UriComponentsBuilder _uriComponentsBuilder;
    private final ObjectMapper mapper;

    @Autowired
    public TrackService(UriComponentsBuilder _uriComponentsBuilder, ObjectMapper mapper) {
        this._uriComponentsBuilder = _uriComponentsBuilder;
        this.mapper = mapper;
    }

    @Override
    public List<Track> search(String value) {
        UriComponentsBuilder uriComponentsBuilder = (UriComponentsBuilder) _uriComponentsBuilder.clone();

        URI uri = uriComponentsBuilder
                .queryParam("method", "track.search")
                .queryParam("track", value)
                .build()
                .toUri();

        ResponseEntity<JsonNode> lastfmResponse = restTemplate.exchange(uri, HttpMethod.GET, null, JsonNode.class);

        try {
            List<Track> rawTracks = mapper.readValue(Objects.requireNonNull(lastfmResponse.getBody())
                            .get("results")
                            .get("trackmatches")
                            .get("track").toString(),
                    mapper.getTypeFactory().constructCollectionType(List.class, Track.class));

            return rawTracks.parallelStream().peek(track -> {
                Track tmp = getInfo(track.getName(), track.getArtist());
                track.setVideoUrl(tmp.getVideoUrl());
                track.setImage(tmp.getImage());
            }).filter(track -> track.getVideoUrl() != null).collect(Collectors.toList());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Track getInfo(String name, String artist) {
        UriComponentsBuilder uriComponentsBuilder = (UriComponentsBuilder) _uriComponentsBuilder.clone();

        URI uri = uriComponentsBuilder
                .queryParam("method", "track.getInfo")
                .queryParam("track", name)
                .queryParam("artist", artist)
                .build()
                .toUri();

        try {
            ResponseEntity<JsonNode> lastfmResponse = restTemplate.exchange(uri, HttpMethod.GET, null, JsonNode.class);
            Track track = mapper.readValue(Objects.requireNonNull(lastfmResponse.getBody())
                            .get("track").toString(),
                    Track.class);

            ExtraTrackInfo extraTrackInfo = retrieveYoutubeLinkAndImage(track.getUrl());

            if (extraTrackInfo != null) {
                track.setVideoUrl(extraTrackInfo.getYoutubeUrl());

                if (extraTrackInfo.getImageUrl() != null) {
                    track.setImage(extraTrackInfo.getImageUrl());
                }
            }

            System.out.println(track);

            return track;
        } catch (JsonProcessingException | HttpClientErrorException e) {
            return new Track();
        }
    }

    private ExtraTrackInfo retrieveYoutubeLinkAndImage(String trackUrl) {
        try {
            Document webPage = Jsoup.connect(trackUrl).get();

            List<Element> links = webPage.getElementsByClass("header-new-playlink");
            List<Element> images = webPage.getElementsByClass("header-new-background-image");

            ExtraTrackInfo extraTrackInfo = new ExtraTrackInfo();

            if (links.size() > 0) {
                extraTrackInfo.setYoutubeUrl(links.get(0).attr("href"));
            }

            if (images.size() > 0) {
                String style = images.get(0).attr("style");

                String pattern = "background-image: url\\((.+)\\);";
                Pattern p = Pattern.compile(pattern);
                Matcher m = p.matcher(style);

                if (m.find()) {
                    extraTrackInfo.setImageUrl(m.group(1));
                } else {
                    extraTrackInfo.setImageUrl(null);
                }
            }

            return extraTrackInfo;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Data
    private static class ExtraTrackInfo {
        private String youtubeUrl;
        private String imageUrl;
    }
}
