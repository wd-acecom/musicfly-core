package com.acecom.musicfly.service;

import com.acecom.musicfly.domain.Track;

import java.util.List;

public interface ITrackService {
    List<Track> search(String value);

    Track getInfo(String name, String artist);
}
