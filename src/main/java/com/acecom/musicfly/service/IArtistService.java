package com.acecom.musicfly.service;

import com.acecom.musicfly.domain.Album;
import com.acecom.musicfly.domain.Artist;
import com.acecom.musicfly.domain.Track;

import java.util.List;

public interface IArtistService {
    List<Artist> search(String value);

    Artist getInfo(String name);

    List<Album> getTopAlbums(String artist);

    List<Track> getTopTracks(String artist);
}
