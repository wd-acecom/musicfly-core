package com.acecom.musicfly.exception.handler;

import com.acecom.musicfly.exception.TestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {TestException.class})
    protected ResponseEntity<Object> handleTestException(RuntimeException ex, WebRequest request) {
        return ResponseEntity.status(HttpStatus.BAD_GATEWAY).body(ex.toString());
    }
}
