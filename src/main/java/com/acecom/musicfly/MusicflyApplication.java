package com.acecom.musicfly;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Objects;

@SpringBootApplication
public class MusicflyApplication {

    @Autowired
    private Environment environment;

    public static void main(String[] args) {
        SpringApplication.run(MusicflyApplication.class, args);
    }

    @Bean
    public static ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.setVisibility(
                VisibilityChecker.Std.defaultInstance().withFieldVisibility(JsonAutoDetect.Visibility.ANY));

        return objectMapper;
    }

    @Bean
    public UriComponentsBuilder getUriComponentsBuilder() {
        return UriComponentsBuilder
                .fromHttpUrl(Objects.requireNonNull(environment.getProperty("lastfm.baseUrl")))
                .queryParam("api_key", environment.getProperty("lastfm.apiKey"))
                .queryParam("format", "json");
    }
}
