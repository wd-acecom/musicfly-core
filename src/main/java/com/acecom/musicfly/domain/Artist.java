package com.acecom.musicfly.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Artist {
    private String name;
    private String image;
    private String summary;

    @JsonProperty("image")
    private void unpackImage(JsonNode image) {
        this.image = image.get(image.size() - 2).get("#text").textValue();
    }

    @JsonProperty("bio")
    private void unpackSummary(JsonNode bio) {
        this.summary = bio.get("summary").textValue();
    }
}
