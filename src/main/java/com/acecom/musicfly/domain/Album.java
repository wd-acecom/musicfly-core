package com.acecom.musicfly.domain;

import com.acecom.musicfly.MusicflyApplication;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.util.Collections;
import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Album {
    private String name;
    private String artist;
    private String image;
    private String summary;
    private List<Track> tracks;

    @JsonProperty("image")
    private void unpackImage(JsonNode image) {
        this.image = image.get(image.size() - 2).get("#text").textValue();
    }

    @JsonProperty("wiki")
    private void unpackSummary(JsonNode wiki) {
        this.summary = wiki.get("summary").textValue();
    }

    @JsonProperty("tracks")
    private void unpackTracks(JsonNode tracks) {
        ObjectMapper mapper = MusicflyApplication.getObjectMapper();

        try {
            this.tracks = mapper.readValue(
                    tracks.get("track").toString(),
                    mapper.getTypeFactory().constructCollectionType(List.class, Track.class));
        } catch (JsonProcessingException e) {
            try {
                this.tracks = Collections.singletonList(mapper.readValue(
                        tracks.get("track").toString(), Track.class));
            } catch (JsonProcessingException subError) {
                subError.printStackTrace();
            }
        }
    }

    @JsonProperty("artist")
    private void unpackArtistIfNeeded(JsonNode artist) {
        if (artist.get("name") != null) {
            this.artist = artist.get("name").textValue();
        } else {
            this.artist = artist.textValue();
        }
    }
}
