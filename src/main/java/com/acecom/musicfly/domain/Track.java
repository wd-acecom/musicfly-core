package com.acecom.musicfly.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class Track {
    private String artist;
    private String duration;
    private String name;
    private String image;
    private String url;
    private String videoUrl;

    @JsonProperty("artist")
    private void unpackArtistIfNeeded(JsonNode artist) {
        if (artist.get("name") != null) {
            this.artist = artist.get("name").textValue();
        } else {
            this.artist = artist.textValue();
        }
    }

    @JsonProperty("album")
    private void unpackImageFromAlbum(JsonNode album) {
        if (album != null) {
            JsonNode image = album.get("image");
            this.image = image.get(image.size() - 1).get("#text").textValue();
        }
    }

    @JsonProperty("image")
    private void unpackImage(JsonNode image) {
        if (image != null) {
            this.image = image.get(image.size() - 1).get("#text").textValue();
        }
    }
}
