# Musicfly Core

This project is the backend for the service of [Musicfly](https://musicfly.netlify.app)

## How to run

This project require java 11 for their execution.

To run this project, put on the root folder and run the next command:

### `./mvnw spring-boot:run`

## How to Build

To build this project and run in another server execute the following command

### `./mvnw clean package` 

and deploy the jar on the service of your preference